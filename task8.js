 $(document).ready(function() {
	 	 $('#handleClick').click(function() {

    var firstname = $('#firstname').val();
    var middlename = $('#middlename').val();
    var lastname = $('#lastname').val();
    var email = $('#email').val();
	var password = $('#password').val();
	var confirmpassword = $('#confirmpassword').val();
	var mobile = $('#mobile').val();
	var state = $('#state').val();
	var city = $('#city').val();
	var address = $('#address').val();
	
    

    if (firstname.length < 1) {
       $('#fnameerror').text('Please fill Firstname');
	   $("#fnameerror").css("color", "red");
	    return false;
    }
    if (middlename.length < 1) {
       $('#mnameerror').text('Please fill Middlename');
	   $("#mnameerror").css("color", "red");
	  return false;
	     }
    if(lastname.length < 1) {
       $('#lnameerror').text('Please fill lastname');
	   $("#lnameerror").css("color", "red");
	  return false;
    }

    if (email.length < 1) {
       $('#emailerror').text('Email cant be Empty');
	   $("#emailerror").css("color", "red"); 
	  return false;
    }

    if (password=='') {
      $('#passwordrror').text('Password can not be Empty');
	  $("#passworderror").css("color", "red");
	  return false;
    }

    if (confirmpassword=='') {
      $('#confirmpassworderror').text('Confirm Password cant be Empty');
	  $("#confirmpassworderror").css("color", "red");
	  return false;
    } 

    if (mobile.length < 1) {
     $('#mobileerror').text('Mobile cant be Empty');
	 $("#mobileerror").css("color", "red"); 
	  return false;
    }

	if(address.length < 1) {
       $('#addresserror').text('Address cant be Empty');
	   $("#addresserror").css("color", "red"); 
	  return false;
    }

	if (state=='') {
      $('#stateerror').text('State cant be Empty');
	  $("#stateerror").css("color", "red"); 
	  return false;
    }

	if (city=='') {
      $('#cityerror').text('City cant be Empty');
	  $("#cityerror").css("color", "red"); 
	  return false;
    }

	if (address.length < 1) {
      $('#addresserror').text('Zipcode cant be Empty');
	  $("#addresserror").css("color", "red"); 
	  return false;
    }

	else{

        var data = { 
        "firstName": firstName,
        "middleName": middleName,
        "lastName" : lastName,
        "email": email,
        "mobile": mobile,
        "state": state,
        "city": city,
        "address": address  
        }; 
      
      // Here unique id is size of our total entry +1.  

    localStorage.setItem((localStorage.length)+1,JSON.stringify(data));
    }
  });

});