
function validateForm() {
  
  var firstName = document.forms["myForm"]["firstName"].value;
  if(firstName == ""){
      document.getElementById("fnameerror").innerHTML = "*Please fill First Name";
      return false;
  }
  
  var middleName = document.forms["myForm"]["middleName"].value;
  if(middleName == ""){
      document.getElementById("mnameerror").innerHTML = "Please fill Middle Name"
      return false;
  }

  var lastName = document.forms["myForm"]["lastName"].value;
  if(lastName == ""){
    document.getElementById("lnameerror").innerHTML="Please fill Last Name";
      return false;
  }

  var email = document.forms["myForm"]["email"].value;
  if(email == ""){
      document.getElementById("emailerror").innerHTML = "Please fill email"
      return false;
  }

  var mobile = document.forms["myForm"]["mobile"].value;
  if(mobile == ""){
     document.getElementById("mobileerror").innerHTML = "Please fill Mobile Number";
      return false;
  }

  var state = document.forms["myForm"]["state"].value;
  if(state == ""){
     document.getElementById("stateerror").innerHTML = "Please fill State";
      return false;
  }

  var city = document.forms["myForm"]["city"].value;
  if(city == ""){
     document.getElementById("cityerror").innerHTML = "Please fill City";
      return false;
  }
  
  var password = document.forms["myForm"]["password"].value;
  if(password == ""){
     document.getElementById("passworderror").innerHTML = "Please fill Password";
      return false;
  }

  var confirmPassword = document.forms["myForm"]["confirmPassword"].value;
  if(confirmPassword == ""){
     document.getElementById("confirmpassworderror").innerHTML = "Please fill Confirm Password";
      return false;
  }
  
  var higherEducation = document.forms["myForm"]["higherEducation"].value;
  if(higherEducation == ""){
      document.getElementById("highereducationerror").innerHTML = "Please fill Mobile Number";
      return false;
  }

  var address = document.forms["myForm"]["address"].value;
  if(address == ""){
     document.getElementById("addresserror").innerHTML = "Please fill Address";
       return false;
   }  
   return true;
}


function validatePassword(){

  var password = document.forms["myForm"]["password"].value;
  
  var numeric=0,alpha=0,special=0;
  for(let i=0;i<password.length;i++){
     if(password[i]>=0 && password[i]<=9) numeric++;
     else if(password[i]>='a' && password[i]<='z' || password[i]>='A' && password[i]<='Z') alpha++;
     else special++;
  }
  
  if(password.length < 8){
      document.getElementById("passworderror").innerHTML = "Length of password is less than eight character";
      return false;
  }
  if(numeric==0 || alpha==0 || special==0){
    document.getElementById("passworderror").innerHTML ="Password must contain atleast one numric, alphabates and one special character";
      return false;
  }
  return true;
}

function validateConfirmPassword () {
    var password = document.forms["myForm"]["password"].value;
    var confirmPassword = document.forms["myForm"]["confirmPassword"].value;

    if(password!=confirmPassword){
       document.getElementById("samepassworderror").innerHTML = "Password and confirm password do not matches";
        return false;
    }
    return true;
}


function handleStorage (event) {

  
     var firstName = document.forms["myForm"]["firstName"].value;
     var middleName = document.forms["myForm"]["middleName"].value;
     var lastName = document.forms["myForm"]["lastName"].value;
     var email = document.forms["myForm"]["email"].value;
     var mobile = document.forms["myForm"]["mobile"].value;
     var higherEducation = document.forms["myForm"]["higherEducation"].value;
     var address = document.forms["myForm"]["address"].value;
     var password = document.forms["myForm"]["password"].value;

     
     
    var data = { 
        "firstName": firstName,
        "middleName": middleName,
        "lastName" : lastName,
        "email": email,
        "mobile": mobile,
        "higherEducation": higherEducation,
        "address": address  
        }; 
        
    localStorage.setItem((localStorage.length)+1,JSON.stringify(data));
}


function handleSubmit(){
if(validateConfirmPassword() == true && validateForm() == true && validatePassword() == true){
   
   handleStorage();
}
}




function deleteElement (id){
   
   console.log(id);  
   localStorage.removeItem(id);

}

function EditElement(id){
   
  //console.log(id);
    var value = localStorage.getItem(key);
    var myData = JSON.parse(value);

    document.forms["myForm"]["firstName"].value=myData.firstName;
    document.forms["myForm"]["middleName"].value=myData.middleName;
    document.forms["myForm"]["lastName"].value=myData.lastName;
    document.forms["myForm"]["email"].value=myData.email;
    document.forms["myForm"]["password"].value=myData.password;
    document.forms["myForm"]["confirmPassword"].value=myData.confirmpassword;
    document.forms["myForm"]["mobile"].value=myData.mobile;
    document.forms["myForm"]["address"].value=myData.address;


}