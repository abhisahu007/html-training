
function validateForm() {
  
  var firstName = document.forms["myForm"]["firstName"].value;
  if(firstName == ""){
      alert("Please fill First Name");
      return false;
  }
  
  var middleName = document.forms["myForm"]["middleName"].value;
  if(middleName == ""){
      alert("Please fill Middle Name");
      return false;
  }

  var lastName = document.forms["myForm"]["lastName"].value;
  if(lastName == ""){
      alert("Please fill Last Name");
      return false;
  }

  var email = document.forms["myForm"]["email"].value;
  if(email == ""){
      alert("Please fill Email");
      return false;
  }

  var mobile = document.forms["myForm"]["mobile"].value;
  if(mobile == ""){
      alert("Please fill Mobile Number");
      return false;
  }
   
  


  var higherEducation = document.forms["myForm"]["higherEducation"].value;
  if(higherEducation == ""){
      alert("Please fill Mobile Number");
      return false;
  }

  var address = document.forms["myForm"]["address"].value;
  if(address == ""){
       alert("Please fill Address");
       return false;
   }  
}


function validatePassword(){

  var password = document.forms["myForm"]["password"].value;
  
  var numeric=0,alpha=0,special=0;
  for(let i=0;i<password.length;i++){
     if(password[i]>=0 && password[i]<=9) numeric++;
     else if(password[i]>='a' && password[i]<='z' || password[i]>='A' && password[i]<='Z') alpha++;
     else special++;
  }
  
  if(password.length < 8){
      alert("Length of password is less than eight character");
      return false;
  }
  if(numeric==0 || alpha==0 || special==0){
      alert("Password must contain atleast one numric, alphabates and one special character");
      return false;
  }
}

function validateConfirmPassword{
    var password = document.forms["myForm"]["password"].value;
    var confirmPassword = document.forms["myForm"]["confirmPassword"].value;

    if(password!=confirmPassword){
        alert("Password and confirm password do not matches");
        return false;
    }
}